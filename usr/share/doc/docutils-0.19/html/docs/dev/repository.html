<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />
<title>The Docutils Version Repository</title>
<meta name="author" content="Lea Wiemann, Docutils developers" />
<meta name="date" content="2022-04-02" />
<meta name="copyright" content="This document has been placed in the public domain." />
<link rel="stylesheet" href="../../html4css1.css" type="text/css" />
</head>
<body>
<div class="header">
<a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../index.html">Overview</a> | <a class="reference external" href="../index.html#project-fundamentals">About</a> | <a class="reference external" href="../index.html#user">Users</a> | <a class="reference external" href="../index.html#ref">Reference</a> | <a class="reference external" href="../index.html#howto">Developers</a>
<hr class="header"/>
</div>
<div class="document" id="the-docutils-version-repository">
<h1 class="title">The <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> Version Repository</h1>
<table class="docinfo" frame="void" rules="none">
<col class="docinfo-name" />
<col class="docinfo-content" />
<tbody valign="top">
<tr><th class="docinfo-name">Author:</th>
<td>Lea Wiemann, Docutils developers</td></tr>
<tr><th class="docinfo-name">Contact:</th>
<td><a class="first last reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></td></tr>
<tr><th class="docinfo-name">Revision:</th>
<td>9051</td></tr>
<tr><th class="docinfo-name">Date:</th>
<td>2022-04-02</td></tr>
<tr><th class="docinfo-name">Copyright:</th>
<td>This document has been placed in the public domain.</td></tr>
</tbody>
</table>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-diretories.

Attention: this is not a standalone document. -->
<div class="admonition admonition-quick-instructions">
<p class="first admonition-title">Quick Instructions</p>
<p>To get a checkout of the Docutils source tree (with the
sandboxes) with <a class="reference external" href="https://subversion.apache.org/">SVN</a>, type</p>
<pre class="literal-block">
svn checkout https://svn.code.sf.net/p/docutils/code/trunk docutils-code
</pre>
<p>Users of <a class="reference external" href="http://git-scm.com/">Git</a> can clone a mirror of the docutils repository with</p>
<pre class="literal-block">
git clone git://repo.or.cz/docutils.git
</pre>
<p class="last">If you are going to commit changes to the repository, please read
the <strong>whole document</strong>, especially the section &quot;<a class="reference internal" href="#information-for-developers">Information for
Developers</a>&quot;!</p>
</div>
<p>Docutils uses a <a class="reference external" href="https://subversion.apache.org/">Subversion</a> (SVN) repository located at
<tt class="docutils literal">docutils.svn.sourceforge.net</tt>.</p>
<p>While Unix and Mac OS X users will probably prefer the standard
Subversion command line interface, Windows user may want to try
<a class="reference external" href="https://tortoisesvn.net/">TortoiseSVN</a>, a convenient explorer extension.  The instructions apply
analogously.</p>
<p>There is a <a class="reference external" href="http://git-scm.com/">Git</a> mirror at <a class="reference external" href="http://repo.or.cz/docutils.git">http://repo.or.cz/docutils.git</a> providing
<a class="reference internal" href="#web-access">web access</a> and the base for <a class="reference internal" href="#creating-a-local-git-clone">creating a local Git clone</a>.
<a class="footnote-reference" href="#github-mirrors" id="footnote-reference-1">[1]</a></p>
<p>For the project policy on repository use (check-in requirements,
branching, etc.), please see the <a class="reference external" href="policies.html#subversion-repository">Docutils Project Policies</a>.</p>
<div class="contents topic" id="contents">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#accessing-the-repository" id="toc-entry-1">Accessing the Repository</a><ul>
<li><a class="reference internal" href="#web-access" id="toc-entry-2">Web Access</a></li>
<li><a class="reference internal" href="#repository-access-methods" id="toc-entry-3">Repository Access Methods</a></li>
<li><a class="reference internal" href="#checking-out-the-repository" id="toc-entry-4">Checking Out the Repository</a></li>
<li><a class="reference internal" href="#switching-the-repository-root" id="toc-entry-5">Switching the Repository Root</a></li>
</ul>
</li>
<li><a class="reference internal" href="#editable-installs" id="toc-entry-6">Editable installs</a></li>
<li><a class="reference internal" href="#information-for-developers" id="toc-entry-7">Information for Developers</a><ul>
<li><a class="reference internal" href="#setting-up-your-subversion-client-for-development" id="toc-entry-8">Setting Up Your Subversion Client For Development</a></li>
</ul>
</li>
<li><a class="reference internal" href="#repository-layout" id="toc-entry-9">Repository Layout</a></li>
</ul>
</div>
<div class="section" id="accessing-the-repository">
<h1><a class="toc-backref" href="#toc-entry-1">Accessing the Repository</a></h1>
<div class="section" id="web-access">
<h2><a class="toc-backref" href="#toc-entry-2">Web Access</a></h2>
<p>The repository can be browsed and examined via the web at
<a class="reference external" href="https://sourceforge.net/p/docutils/code">https://sourceforge.net/p/docutils/code</a>.</p>
<p>Alternatively, use the web interface at <a class="reference external" href="http://repo.or.cz/docutils.git">http://repo.or.cz/docutils.git</a>.
<a class="footnote-reference" href="#github-mirrors" id="footnote-reference-2">[1]</a></p>
<table class="docutils footnote" frame="void" id="github-mirrors" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label">[1]</td><td><em>(<a class="fn-backref" href="#footnote-reference-1">1</a>, <a class="fn-backref" href="#footnote-reference-2">2</a>)</em> There are also 3rd-party mirrors and forks at
GitHub, some of them orphaned. At the time of this writing (2021-11-03),
<a class="reference external" href="https://github.com/live-clones/docutils/tree/master/docutils">https://github.com/live-clones/docutils/tree/master/docutils</a>
provides an hourly updated clone.</td></tr>
</tbody>
</table>
</div>
<div class="section" id="repository-access-methods">
<h2><a class="toc-backref" href="#toc-entry-3">Repository Access Methods</a></h2>
<p>To get a checkout, first determine the root of the repository depending
on your preferred protocol:</p>
<dl class="docutils">
<dt>anonymous access: (read only)</dt>
<dd><p class="first"><a class="reference external" href="https://subversion.apache.org/">Subversion</a>: <tt class="docutils literal"><span class="pre">https://svn.code.sf.net/p/docutils/code</span></tt></p>
<p class="last"><a class="reference external" href="http://git-scm.com/">Git</a>: <tt class="docutils literal"><span class="pre">git://repo.or.cz/docutils.git</span></tt></p>
</dd>
<dt><a class="reference internal" href="#developer-access">developer access</a>: (read and write)</dt>
<dd><tt class="docutils literal"><span class="pre">svn+ssh://&lt;USERNAME&gt;&#64;svn.code.sf.net/p/docutils/code</span></tt></dd>
</dl>
</div>
<div class="section" id="checking-out-the-repository">
<h2><a class="toc-backref" href="#toc-entry-4">Checking Out the Repository</a></h2>
<p id="creating-a-local-git-clone"><a class="reference external" href="http://git-scm.com/">Git</a> users can clone a mirror of the docutils repository with</p>
<pre class="literal-block">
git clone git://repo.or.cz/docutils.git
</pre>
<p>and proceed according to the <a class="reference external" href="https://git.wiki.kernel.org/index.php/GitDocumentation">Git documentation</a>.
Developer access (read and write) is possible with <a class="reference external" href="https://git.wiki.kernel.org/index.php/Git-svn">git svn</a>.</p>
<p><a class="reference external" href="https://subversion.apache.org/">Subversion</a> users can use the following commands
(substitute your preferred repository root for ROOT):</p>
<ul>
<li><p class="first">To check out only the current main source tree of Docutils, type</p>
<pre class="literal-block">
svn checkout ROOT/trunk/docutils
</pre>
</li>
<li><p class="first">To check out everything (main tree, sandboxes, web site, and parallel
projects), type</p>
<pre class="literal-block">
svn checkout ROOT/trunk docutils
</pre>
<p>This will create a working copy of the whole trunk in a new directory
called <tt class="docutils literal">docutils</tt>.</p>
</li>
</ul>
<p>Note that you probably do <em>not</em> want to check out the ROOT itself
(without &quot;/trunk&quot;), because then you'd end up fetching the whole
Docutils tree for every branch and tag over and over again.</p>
<p>To update your working copy later on, <tt class="docutils literal">cd</tt> into the working copy and
type</p>
<pre class="literal-block">
svn update
</pre>
</div>
<div class="section" id="switching-the-repository-root">
<h2><a class="toc-backref" href="#toc-entry-5">Switching the Repository Root</a></h2>
<p>If you changed your mind and want to use a different repository root,
<tt class="docutils literal">cd</tt> into your working copy and type:</p>
<pre class="literal-block">
svn switch --relocate OLDROOT NEWROOT
</pre>
</div>
</div>
<div class="section" id="editable-installs">
<h1><a class="toc-backref" href="#toc-entry-6">Editable installs</a></h1>
<p>There are several ways to ensure that edits to the Docutils code are
picked up by Python.
We'll assume that the Docutils &quot;trunk&quot; is checked out under the
<tt class="docutils literal">~/projects/</tt> directory.</p>
<ol class="arabic">
<li><p class="first">Do an <a class="reference external" href="https://pip.pypa.io/en/stable/cli/pip_install/#editable-installs">editable install</a> with <a class="reference external" href="https://pypi.org/project/pip/">pip</a>:</p>
<pre class="literal-block">
python3 -m pip install -e ~/projects/docutils/docutils
</pre>
</li>
<li><p class="first">Install in <a class="reference external" href="https://setuptools.pypa.io/en/latest/userguide/development_mode.html#development-mode">development mode</a> with <a class="reference external" href="https://pypi.org/project/setuptools/">setuptools</a>.</p>
</li>
<li id="install-manually"><p class="first">Install &quot;manually&quot;.</p>
<p>Ensure that the &quot;docutils&quot; package is in <tt class="docutils literal">sys.path</tt> by
one of the following actions:</p>
<ul>
<li><p class="first">Set the <tt class="docutils literal">PYTHONPATH</tt> environment variable so that Python
picks up your local working copy of the code.</p>
<p>For the bash shell, add this to your <tt class="docutils literal"><span class="pre">~/.profile</span></tt>:</p>
<pre class="literal-block">
PYTHONPATH=$HOME/projects/docutils/docutils
export PYTHONPATH
</pre>
<p>The first line points to the directory containing the <tt class="docutils literal">docutils</tt>
package.  The second line exports the environment variable.</p>
</li>
<li><p class="first">Create a symlink to the docutils package directory somewhere in the
module search path (<tt class="docutils literal">sys.path</tt>), e.g.,</p>
<pre class="literal-block">
ln -s ~/projects/docutils/docutils \
      /usr/local/lib/python3.9/dist-packages/
</pre>
</li>
<li><p class="first">Use a <a class="reference external" href="https://docs.python.org/library/site.html">path configuration file</a>.</p>
</li>
</ul>
<p>Optionally, add some or all <a class="reference external" href="../user/tools.html">front-end tools</a>
to the binary search path, e.g.,
add the <tt class="docutils literal">tools</tt> directory to the <tt class="docutils literal">PATH</tt> variable:</p>
<pre class="literal-block">
PATH=$PATH:$HOME/projects/docutils/docutils/tools
export PATH
</pre>
<p>or link idividual front-end tools to a suitable place
in the binary path:</p>
<pre class="literal-block">
ln -s ~/projects/docutils/docutils/tools/docutils-cli.py \
      /usr/local/bin/docutils
</pre>
</li>
</ol>
<ol class="arabic" start="5">
<li><p class="first">Reinstall Docutils after any change:</p>
<pre class="literal-block">
python3 setup.py install
</pre>
<div class="admonition caution">
<p class="first admonition-title">Caution!</p>
<p>This method is <strong>not</strong> recommended for day-to-day development;
it's too easy to forget.  Confusion inevitably ensues.</p>
<p class="last">If you install Docutils this way, Python will always pick up the
last-installed copy of the code.  If you ever forget to
reinstall the &quot;docutils&quot; package, Python won't see your latest
changes.</p>
</div>
</li>
</ol>
<p>A useful addition to the <tt class="docutils literal">docutils</tt> top-level directory in branches
and alternate copies of the code is a <tt class="docutils literal"><span class="pre">set-PATHS</span></tt> file
containing the following lines:</p>
<pre class="literal-block">
# source this file
export PYTHONPATH=$PWD:$PWD
export PATH=$PWD/tools:$PATH
</pre>
<p>Open a shell for this branch, <tt class="docutils literal">cd</tt> to the <tt class="docutils literal">docutils</tt> top-level
directory, and &quot;source&quot; this file.  For example, using the bash
shell:</p>
<pre class="literal-block">
$ cd some-branch/docutils
$ . set-PATHS
</pre>
</div>
<div class="section" id="information-for-developers">
<span id="developer-access"></span><h1><a class="toc-backref" href="#toc-entry-7">Information for Developers</a></h1>
<p>If you would like to have write access to the repository, register
with <a class="reference external" href="https://sourceforge.net/">SourceForge.net</a> and send your SourceForge.net
user names to <a class="reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a>.
(Note that there may be a delay of several hours until you can commit
changes to the repository.)</p>
<p>Sourceforge SVN access is documented <a class="reference external" href="https://sourceforge.net/p/forge/documentation/svn/">here</a></p>
<p>Ensure any changes comply with the <a class="reference external" href="policies.html">Docutils Project Policies</a>
before <a class="reference external" href="policies.html#check-ins">checking in</a>,</p>
<div class="section" id="setting-up-your-subversion-client-for-development">
<h2><a class="toc-backref" href="#toc-entry-8">Setting Up Your Subversion Client For Development</a></h2>
<p>Before committing changes to the repository, please ensure that the
following lines are contained (and uncommented) in your local
~/.subversion/config file, so that new files are added with the
correct properties set:</p>
<pre class="literal-block">
[miscellany]
# For your convenience:
global-ignores = ... *.pyc ...
# For correct properties:
enable-auto-props = yes

[auto-props]
*.py = svn:eol-style=native;svn:keywords=Author Date Id Revision
*.txt = svn:eol-style=native;svn:keywords=Author Date Id Revision
*.html = svn:eol-style=native;svn:keywords=Author Date Id Revision
*.xml = svn:eol-style=native;svn:keywords=Author Date Id Revision
*.tex = svn:eol-style=native;svn:keywords=Author Date Id Revision
*.css = svn:eol-style=native;svn:keywords=Author Date Id Revision
*.patch = svn:eol-style=native
*.sh = svn:eol-style=native;svn:executable;svn:keywords=Author Date Id Revision
*.png = svn:mime-type=image/png
*.jpg = svn:mime-type=image/jpeg
*.gif = svn:mime-type=image/gif
</pre>
</div>
</div>
<div class="section" id="repository-layout">
<h1><a class="toc-backref" href="#toc-entry-9">Repository Layout</a></h1>
<p>The following tree shows the repository layout:</p>
<pre class="literal-block">
docutils/
|-- branches/
|   |-- branch1/
|   |   |-- docutils/
|   |   |-- sandbox/
|   |   `-- web/
|   `-- branch2/
|       |-- docutils/
|       |-- sandbox/
|       `-- web/
|-- tags/
|   |-- tag1/
|   |   |-- docutils/
|   |   |-- sandbox/
|   |   `-- web/
|   `-- tag2/
|       |-- docutils/
|       |-- sandbox/
|       `-- web/
`-- trunk/
    |-- docutils/
    |-- sandbox/
    `-- web/
</pre>
<p>The main source tree lives at <tt class="docutils literal">docutils/trunk/docutils/</tt>, next to
the sandboxes (<tt class="docutils literal">docutils/trunk/sandbox/</tt>) and the web site files
(<tt class="docutils literal">docutils/trunk/web/</tt>).</p>
<p><tt class="docutils literal">docutils/branches/</tt> and <tt class="docutils literal">docutils/tags/</tt> contain (shallow) copies
of either the whole trunk or only the main source tree
(<tt class="docutils literal">docutils/trunk/docutils</tt>).</p>
</div>
</div>
<div class="footer">
<hr class="footer" />
<a class="reference external" href="repository.txt">View document source</a>.
Generated on: 2022-10-16 21:27 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.

</div>
</body>
</html>
