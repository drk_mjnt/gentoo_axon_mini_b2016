<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />
<title>Docutils Runtime Settings</title>
<meta name="author" content="David Goodger, Günter Milde" />
<meta name="date" content="2022-04-02" />
<meta name="copyright" content="This document has been placed in the public domain." />
<link rel="stylesheet" href="../../html4css1.css" type="text/css" />
</head>
<body>
<div class="header">
<a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../index.html">Overview</a> | <a class="reference external" href="../index.html#project-fundamentals">About</a> | <a class="reference external" href="../index.html#user">Users</a> | <a class="reference external" href="../index.html#ref">Reference</a> | <a class="reference external" href="../index.html#howto">Developers</a>
<hr class="header"/>
</div>
<div class="document" id="docutils-runtime-settings">
<h1 class="title">Docutils Runtime Settings</h1>
<table class="docinfo" frame="void" rules="none">
<col class="docinfo-name" />
<col class="docinfo-content" />
<tbody valign="top">
<tr><th class="docinfo-name">Author:</th>
<td>David Goodger, Günter Milde</td></tr>
<tr><th class="docinfo-name">Contact:</th>
<td><a class="first last reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></td></tr>
<tr><th class="docinfo-name">Date:</th>
<td>2022-04-02</td></tr>
<tr><th class="docinfo-name">Revision:</th>
<td>9051</td></tr>
<tr><th class="docinfo-name">Copyright:</th>
<td>This document has been placed in the public domain.</td></tr>
</tbody>
</table>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-diretories.

Attention: this is not a standalone document. -->
<div class="contents topic" id="contents">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#introduction" id="toc-entry-1">Introduction</a></li>
<li><a class="reference internal" href="#settings-priority" id="toc-entry-2">Settings priority</a></li>
<li><a class="reference internal" href="#settingsspec-base-class" id="toc-entry-3">SettingsSpec base class</a></li>
<li><a class="reference internal" href="#glossary" id="toc-entry-4">Glossary</a><ul>
<li><a class="reference internal" href="#components" id="toc-entry-5">components</a></li>
<li><a class="reference internal" href="#convenience-functions" id="toc-entry-6">convenience functions</a></li>
<li><a class="reference internal" href="#settings-spec" id="toc-entry-7">settings_spec</a></li>
</ul>
</li>
</ul>
</div>
<div class="section" id="introduction">
<h1><a class="toc-backref" href="#toc-entry-1">Introduction</a></h1>
<p>Docutils runtime settings are assembled from several sources:</p>
<ul class="simple">
<li>Settings specifications of the selected <a class="reference internal" href="#components">components</a>,</li>
<li><a class="reference external" href="../user/config.html#configuration-files">configuration files</a> (if enabled), and</li>
<li>command-line options (if enabled).</li>
</ul>
<p>Docutils overlays default and explicitly specified values from these
sources such that settings behave the way we want and expect them to
behave.</p>
</div>
<div class="section" id="settings-priority">
<h1><a class="toc-backref" href="#toc-entry-2">Settings priority</a></h1>
<p>The sources are overlaid in the following order (later sources
overwrite earlier ones):</p>
<ol class="arabic simple">
<li>Defaults specified in the <a class="reference internal" href="#settingsspec-settings-spec">settings_spec</a> and
<a class="reference internal" href="#settingsspec-settings-defaults">settings_defaults</a> attributes for each <a class="reference internal" href="#component">component</a>.</li>
<li>Defaults specified in the <a class="reference internal" href="#settingsspec-settings-default-overrides">settings_default_overrides</a> attribute
for each <a class="reference internal" href="#component">component</a>.</li>
<li>Settings specified in the <a class="reference internal" href="#settings-overrides-parameter">settings_overrides</a> parameter of the
<a class="reference internal" href="#convenience-functions">convenience functions</a> resp. the <cite>settings_overrides</cite> attribute of
a <a class="reference external" href="publisher.html">Publisher</a> instance.</li>
<li>Settings specified in <a class="reference internal" href="#active-sections">active sections</a> of the <a class="reference external" href="../user/config.html#configuration-files">configuration files</a>
in the order described in <a class="reference external" href="../user/config.html#configuration-file-sections-entries">Configuration File Sections &amp; Entries</a>
(if enabled).</li>
<li>Command line options (if enabled).</li>
</ol>
<p>For details see the <tt class="docutils literal">docutils/__init__.py</tt>, <tt class="docutils literal">docutils/core.py</tt>, and
<tt class="docutils literal">docutils.frontend.py</tt> modules and the implementation description in
<a class="reference external" href="../dev/runtime-settings-processing.html">Runtime Settings Processing</a>.</p>
</div>
<div class="section" id="settingsspec-base-class">
<span id="settingsspec"></span><h1><a class="toc-backref" href="#toc-entry-3">SettingsSpec base class</a></h1>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">Implementation details will change with the move to replace the
deprecated <a class="reference external" href="https://docs.python.org/dev/library/optparse.html">optparse</a> module with <a class="reference external" href="https://docs.python.org/dev/library/argparse.html">argparse</a>.</p>
</div>
<p>The <cite>docutils.SettingsSpec</cite> base class is inherited by Docutils
<a class="reference internal" href="#components">components</a> and <cite>frontend.OptionParser</cite>.
It defines the following six <strong>attributes</strong>:</p>
<dl class="docutils" id="settingsspec-settings-spec">
<dt><cite>settings_spec</cite></dt>
<dd><p class="first">a sequence of</p>
<ol class="arabic simple">
<li>option group title (string or None)</li>
<li>description (string or None)</li>
<li>option tuples with<ol class="loweralpha">
<li>help text</li>
<li>options string(s)</li>
<li>dictionary with keyword arguments for <a class="reference external" href="https://docs.python.org/dev/library/optparse.html#optparse.OptionParser.add_option">OptionParser.add_option()</a>
and an optional &quot;validator&quot;, a <cite>frontend.validate_*()</cite> function
that processes the values (e.g. convert to other data types).</li>
</ol>
</li>
</ol>
<p class="last">For examples, see the source of <tt class="docutils literal">frontend.OptionParser.settings_spec</tt>
or the <cite>settings_spec</cite> attributes of the Docutils <a class="reference internal" href="#components">components</a>.</p>
</dd>
<dt id="settingsspec-settings-defaults"><cite>settings_defaults</cite></dt>
<dd>for purely programmatic settings
(not accessible from command line and configuration files).</dd>
<dt id="settingsspec-settings-default-overrides"><cite>settings_default_overrides</cite></dt>
<dd>to override defaults for settings
defined in other components' <cite>setting_specs</cite>.</dd>
<dt><cite>relative_path_settings</cite></dt>
<dd>listing settings containing filesystem paths.</dd>
<dt id="active-sections"><cite>config_section</cite></dt>
<dd>the configuration file section specific to this
component.</dd>
<dt><cite>config_section_dependencies</cite></dt>
<dd>lists configuration files sections
that should also be read (before the <cite>config_section</cite>).</dd>
</dl>
<p>The last two attributes define which configuration file sections are
&quot;active&quot;. See also <a class="reference external" href="../user/config.html#configuration-file-sections-entries">Configuration File Sections &amp; Entries</a>.</p>
</div>
<div class="section" id="glossary">
<h1><a class="toc-backref" href="#toc-entry-4">Glossary</a></h1>
<div class="section" id="components">
<span id="component"></span><h2><a class="toc-backref" href="#toc-entry-5">components</a></h2>
<p>Docutils front-ends and applications combine a selection of
<em>components</em> of the <a class="reference external" href="../peps/pep-0258.html#docutils-project-model">Docutils Project Model</a>.</p>
<p>All components inherit the <a class="reference internal" href="#settingsspec">SettingsSpec</a> base class.
This means that all instances of <tt class="docutils literal">readers.Reader</tt>, <tt class="docutils literal">parsers.Parser</tt>, and
<tt class="docutils literal">writers.Writer</tt> are also instances of <tt class="docutils literal">docutils.SettingsSpec</tt>.</p>
<p>For the determination of runtime settings, <tt class="docutils literal">frontend.OptionParser</tt> and
the <a class="reference internal" href="#settings-spec-parameter">settings_spec parameter</a> in application settings specifications
are treated as components as well.</p>
</div>
<div class="section" id="convenience-functions">
<span id="convenience-function"></span><h2><a class="toc-backref" href="#toc-entry-6">convenience functions</a></h2>
<p>Applications usually deploy Docutils by one of the
<a class="reference external" href="publisher.html#publisher-convenience-functions">Publisher convenience functions</a>.</p>
<p>All convenience functions accept the following optional parameters:</p>
<dl class="docutils" id="settings-parameter">
<dt><cite>settings</cite></dt>
<dd><p class="first">a <tt class="docutils literal">frontend.Values</tt> instance.
If present, it must be complete.</p>
<p class="last">No further runtime settings processing is done and the
following parameters have no effect.</p>
</dd>
<dt id="settings-spec-parameter"><cite>settings_spec</cite></dt>
<dd>a <a class="reference internal" href="#settingsspec">SettingsSpec</a> subclass or instance containing the settings
specification for the &quot;Application&quot; itself.
The instance is added to the <a class="reference internal" href="#components">components</a> (after the generic
settings, parser, reader, and writer).</dd>
<dt id="settings-overrides-parameter"><cite>settings_overrides</cite></dt>
<dd>a dictionary which is used to update the
defaults of the components' settings specifications.</dd>
<dt id="config-section-parameter"><cite>config_section</cite></dt>
<dd><p class="first">the name of an application-specific
<a class="reference external" href="../user/config.html#configuration-file-sections-entries">configuration file section</a> for this application.</p>
<p class="last">Can be specified instead of a <cite>settings_spec</cite> (a new <a class="reference internal" href="#settingsspec">SettingsSpec</a>
instance that just defines a configuration section will be created)
or in addition to a <cite>settings_spec</cite>
(overriding its <cite>config_section</cite> attribute).</p>
</dd>
</dl>
</div>
<div class="section" id="settings-spec">
<h2><a class="toc-backref" href="#toc-entry-7">settings_spec</a></h2>
<p>The name <tt class="docutils literal">settings_spec</tt> may refer to</p>
<ol class="loweralpha simple">
<li>an instance of the <a class="reference internal" href="#settingsspec">SettingsSpec</a> class, or</li>
<li>the data structure <a class="reference internal" href="#settingsspec-settings-spec">SettingsSpec.settings_spec</a> which is used to
store settings details.</li>
</ol>
<!-- References: -->
</div>
</div>
</div>
<div class="footer">
<hr class="footer" />
<a class="reference external" href="runtime-settings.txt">View document source</a>.
Generated on: 2022-10-16 21:27 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.

</div>
</body>
</html>
