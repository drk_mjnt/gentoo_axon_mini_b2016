��    �     �  -  �      �"  -   �"    �"     �#     �#     �#     	$     &$     C$     `$  D   }$  .   �$  I   �$     ;%     X%     r%     �%     �%     �%  $   �%  &   &  ,   .&     [&     x&     �&     �&     �&     �&  ]   �&     O'     m'     �'     �'  "   �'  %   �'  &   
(  !   1(  %   S(  "   y(  #   �(  '   �(      �(     	)     &)     ;)     X)     m)     �)  (   �)  $   �)     �)     �)  #   *     1*     P*     o*     �*  &   �*  %   �*  ,   �*  '    +  H   H+     �+     �+     �+  -   �+  0   �+  .   (,     W,     r,  3   y,  4   �,  -   �,  .   -     ?-  	   H-     R-  :   `-     �-     �-  #   �-  &   �-  &    .     G.     \.  G   e.  @   �.  >   �.  +   -/  =   Y/     �/  "   �/  :   �/     0     0     *0  -   =0  
   k0  #   v0     �0  "   �0  �   �0  E   ]1     �1     �1     �1     �1     2     /2     B2     \2     w2     �2     �2     �2     �2  %   �2     3      3  
   .3     93     K3     _3     q3     �3  "   �3     �3     �3     �3  E   4      ^4  c   4  #   �4  -   5  )   55  �   _5    B6     I7     b7     k7     �7  1   �7  (   �7     �7     8      $8  }  E8     �9  /   �9     :     #:     9:     N:  9   a:  5   �:  .   �:      ;     ;     3;  !   I;     k;     �;     �;     �;  +   �;     �;      <     -<     F<  %   a<     �<     �<  r   !=  >   �=  1   �=  =   >  I   C>  4   �>     �>  #   �>  ?   ?  D   E?  1   �?  %   �?  H   �?  -   +@  H   Y@  �   �@     ,A  B   GA  )   �A  C   �A  .   �A  @   'B  F   hB     �B  *   �C  1   �C  #   D  2   1D  =   dD  0   �D  "   �D  &   �D  %   E  ,   CE  5   pE     �E     �E  _   �E     'F     4F     EF  S   QF  5   �F  4   �F     G     G     4G     BG  
   QG     \G     xG     �G     �G     �G     �G     �G     �G  %   H  2   BH  5   uH     �H  ;   �H  <   �H     7I     MI  &   cI     �I  -   �I  (   �I     �I     J  1   )J     [J     rJ     �J     �J     �J     �J     �J     K     K     )K     IK  $   fK  %   �K     �K     �K     �K     L     L  K   *L  %   vL  $   �L     �L  !   �L     �L     M     !M  %   3M  %   YM  $   M     �M  .   �M     �M     N     N  2   <N     oN     �N  +   �N  $   �N  &   �N  $   O     <O     KO     OO  G   fO  H   �O  B   �O     :P     XP     vP     �P     �P     �P     �P  	   Q     Q     (Q     AQ     [Q     vQ     �Q  '   �Q     �Q     �Q  (   �Q      R     #R  
   AR     LR     jR     mR  &   �R     �R     �R  !   �R     �R     S     %S  %   DS     jS  &   �S  )   �S     �S     �S     T     /T     LT     gT     T  "   �T     �T  *   �T  !   �T  $   !U     FU     IU     NU  I   ]U     �U     �U  #   �U  #   �U     V     /V     FV     TV  #   sV     �V  3   �V  %   �V  %   W     (W  
   7W     BW     UW  "   hW     �W     �W      �W  5   �W  $   �W     $X  4   ?X  %   tX  *   �X  0   �X     �X  .   �X  *   .Y  �   YY  +   �Y  /   Z  %   FZ  -   lZ  /   �Z  ,   �Z  "   �Z  )   [     D[     c[      ~[     �[     �[     �[  #   �[      \     %\     8\     R\     e\  6   }\      �\     �\     �\     �\  -   ]     ;]     U]     t]     �]     �]     �]  7   �]      �]  $   �]     $^  '   D^  >  l^  O   �_     �_     a  !   6a  !   Xa  %   za  %   �a     �a  "   �a  =   	b  2   Gb  P   zb  "   �b     �b  (   c  '   6c     ^c     |c  )   �c  '   �c  ,   �c  &   d     8d  &   Pd     wd      �d  &   �d  q   �d     Ge     `e  &   ye     �e  /   �e  ,   �e  0   f  2   Hf  *   {f  "   �f  4   �f  .   �f  %   -g      Sg      tg  +   �g  *   �g  $   �g     h  B   'h  ;   jh  !   �h     �h  "   �h  0   i  &   3i  &   Zi     �i  &   �i  '   �i  0   �i  %   j  Z   8j     �j     �j  #   �j  2   �j  5   k  @   <k     }k     �k  <   �k  H   �k  9   *l  ,   dl     �l     �l     �l  9   �l     �l     m  $   ,m  '   Qm  &   ym     �m     �m  H   �m  =   n  0   Pn  @   �n  H   �n     o  ,   o  9   Lo     �o     �o     �o  =   �o     p  #   p     7p  2   >p  �   qp  W    q     Xq     uq  #   �q     �q  &   �q     �q  %   r  &   ,r     Sr     pr     �r     �r     �r  B   �r     s  :   0s  
   ks     vs     �s  0   �s  4   �s     t  B   1t  /   tt     �t  !   �t  E   �t  ,   $u  ~   Qu  /   �u  '    v  /   (v  R  Xv  L  �w     �x     y  *   ,y     Wy  4   ky  >   �y     �y  +   �y  (   %z  �  Nz     �{  6   �{     .|     N|     m|     �|  @   �|  @   �|  -   }  @   I}  4   �}     �}      �}     �}     ~  %   1~     W~  4   g~  '   �~  4   �~  '   �~  (   !  5   J     �  �   �  �   .�  G   ��  ;   �  E   C�  U   ��  >   ߁  1   �  *   P�  G   {�  L   Â  9   �  7   J�  N   ��  +   у  J   ��  �   H�  &   ބ  M   �  1   S�  O   ��  ,   Յ  L   �  X   O�  �   ��  '   ��  4   ˇ  .    �  S   /�  ;   ��  ;   ��  &   ��  .   "�  9   Q�  2   ��  2   ��     �     �  {   �     ��     ��     ��  e   Ǌ  3   -�  "   a�     ��     ��  #   ��     ݋     �  0   �  1   6�  $   h�  ,   ��     ��     ь     ڌ  1   �  8   &�  7   _�  4   ��     ̍  :   �  b    �  +   ��     ��  0   ��     �  <   �  5   J�     ��  )   ��  H   ��  *   	�     4�     T�     o�  +   ��     ��     Ȑ  )   �     �     "�     ?�  &   ]�  &   ��      ��     ̑  '   ߑ     �  (   �  V   E�  /   ��  6   ̒     �  !   "�     D�  )   M�     w�  C   ��  )   ړ  ,   �  &   1�  <   X�  4   ��  '   ʔ      �  I   �     ]�     }�  H   ��  5   �  3   �  4   P�     ��     ��  !   ��  p   ��  p   /�  a   ��  -   �  "   0�  !   S�  )   u�  /   ��  +   Ϙ  -   ��     )�  *   D�  %   o�  *   ��  &   ��  #   �  '   �     3�     P�     p�  5   ��     ��  )   ��  
   �  &   ��     �  -   !�  <   O�     ��     ��  ;   ě  7    �     8�  ;   P�  F   ��  *   Ӝ  !   ��  +    �     L�     l�  <   ��  "   ȝ     �  #   �  )   /�  #   Y�  '   }�  9   ��  .   ߞ  6   �     E�  	   J�     T�  b   h�  '   ˟     �  3   
�  1   >�     p�     ��     ��  (   ��  )   �  %   �  ,   1�  -   ^�  ,   ��     ��     Ρ     �  %   ��  !   #�     E�     _�  1   k�  P   ��  0   �     �  V   >�  !   ��  .   ��  6   �     �  5   +�  1   a�  �   ��  9   .�  F   h�  )   ��  2   ٥  9   �  /   F�  :   v�  @   ��  2   �  2   %�  8   X�  .   ��  #   ��     �  +   ��  3   �     M�     h�     ��  "   ��  F   ʨ  +   �     =�     [�  +   o�  =   ��  9   ٩     �  '   3�  +   [�     ��     ��  2   ��  -   ê  0   �  (   "�  '   K�     O  I       �       -  �           u   h      �   .          �      �      %   ~   '             s         F  �   X   :               �   �   9   G   {  �   �       a  �   1           ?   /   2  �   +  �   T         _  �   J            �   n      �   �   �  D   �       .       Q                  d      l     �  E   f   �      [            U   N   w          (  �  u  _   i  �     �  �           2           8       ,     �   q   �   7         +       @   o     �   �   �   �   Y   1  �  e   �   c  �   N      S   x  �   �   �   �   �         �   d      �               �          <  4         �   �   6        �  a          p  n   6       �   b            �   z       �   �           R     �       t  K  M   $   �  =   }          �       y   �   �  �      k      G         W       ]  S          x   A  �   ;  �   X    ^   �   �  �   �   j      �   )  �   }  "   �       &  f  �       O       j  �   �                w   �       �   �   C  �         :  �      �       �   �   L  E      >   �   �   �   `   ;           �   "  �  	      =  �   �       (   [  &           �   �          P  Y          Z  �   v  �   �       �   �   �           5   {         �   7         �   	   4   �   �       �       m  r   �   z  A   �   �   \  |  U  �   �   ]         g         �   >          �   g       �   *              �   0            �   �  �   3  �   �  K       !  
  ~      Z   #  �   l  t           B  <      �       �  F         �   �       �        /  �   9  �           �          �      \   0     �   W  P   D      -           ?  )      �   V   q      m   �   
   #   V      i                 �   s       �      M                B   �           %     �       !   I  �       �             o   @  k     �       L       H       Q   �   �   �   �      �   �     $  �  �  �  �      �   `  �   �  |   �       �   �              '           �           �      �      y  *   5  b   ^    �   �  �  h   �       ,   J   v   C           8  e      H      T  p     �       c                 �   �      r  3     R       �     
Enter the user ID.  End with an empty line:  
Pick an image to use for your photo ID.  The image must be a JPEG file.
Remember that the image is stored within your public key.  If you use a
very large picture, your key will become very large as well!
Keeping the image close to 240x288 is a good size to use.
 
Supported algorithms:
               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
         new signatures: %lu
       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
      Subkey fingerprint:    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (2) I have done casual checking.%s
    (3) I have done very careful checking.%s
    new key revocations: %lu
   Unable to sign.
   secret keys imported: %lu
  (non-exportable)  Primary key fingerprint:  secret keys unchanged: %lu
 # List of assigned trustvalues, created %s
# (Use "gpg --import-ownertrust" to restore them)
 %s does not yet work with %s
 %s makes no sense with %s!
 %s not allowed with %s!
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 (No description given)
 (Probably you want to select %d here)
 (This is a sensitive revocation key)
 (unless you specify the key by fingerprint)
 --output doesn't work for this command
 @
(See the man page for a complete listing of all commands and options)
 @
Options:
  @Commands:
  ASCII armored output forced.
 Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Can't check signature: %s
 Cancel Certificates leading to an ultimately trusted key:
 Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Changing expiration time for the primary key.
 Cipher:  Comment:  Compression:  Create a revocation certificate for this signature? (y/N)  Critical signature notation:  Critical signature policy:  Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this unknown signature? (y/N/q) Detached signature.
 Digest:  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  Email address:  Enter JPEG filename for photo ID:  Enter an optional description; end it with an empty line:
 Enter new filename Enter passphrase
 Enter passphrase:  Enter the user ID of the designated revoker:  Features:  Go ahead and type your message ...
 Hash:  Hint: Select the user IDs to sign
 How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 IDEA cipher unavailable, optimistically attempting to use %s instead
 Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid selection.
 Is this photo correct (y/N/q)?  Key available at:  Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Need the secret key to do this.
 NnCcEeOoQq No help available No reason specified No such user ID.
 No user ID with index %d
 Not a valid email address
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Please correct the error first
 Please don't put the email address into the real name or the comment
 Please enter name of data file:  Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please select exactly one user ID.
 Please select the reason for the revocation:
 Please select what kind of key you want:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Primary key fingerprint: Pubkey:  Public key is disabled.
 Real name:  Really create the revocation certificates? (y/N)  Really delete this self-signature? (y/N) Reason for revocation: %s
 Requested keysize is %u bits
 Revocation certificate created.
 Revocation certificate created.

Please move it to a medium which you can hide away; if Mallory gets
access to this certificate he can use it to make your key unusable.
It is smart to print this certificate and store it away, just in case
your media become unreadable.  But have some caution:  The print system of
your machine might store the data and make it available to others!
 Secret key is available.
 Secret parts of primary key are not available.
 Signature expired %s
 Signature expires %s
 Signature notation:  Signature policy:  The self-signature on "%s"
is a PGP 2.x-style signature.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This signature expired on %s.
 To be revoked by:
 Total number processed: %lu
 Uncompressed Usage: gpgv [options] [files] (-h for help) User ID "%s" is expired. User ID "%s" is not self-signed. User ID "%s" is revoked. User ID is no longer valid WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its designated revoker!
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: appointing a key as a designated revoker cannot be undone!
 WARNING: encrypted message has been manipulated!
 WARNING: invalid notation data found
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: no user ID has been marked as primary.  This command may
              cause a different user ID to become the assumed primary.
 WARNING: nothing exported
 WARNING: potentially insecure symmetrically encrypted session key
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: signature digest conflict in message
 WARNING: this key might be revoked (revocation key not present)
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 You are about to revoke these signatures:
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You must select at least one key.
 You must select at least one user ID.
 You selected this USER-ID:
    "%s"

 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [revocation] [self-signature] [uncertain] a notation name must have only printable characters or spaces, and end with an '='
 a notation value must not use any control characters
 a user notation name must contain the '@' character
 add a photo ID add a revocation key add a user ID armor header:  armor: %s
 assume no on most questions assume yes on most questions assuming %s encrypted data
 batch mode: never ask be somewhat more quiet binary build_packet failed: %s
 can't disable core dumps: %s
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't use a symmetric ESK packet due to the S2K mode
 cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 change the ownertrust change the passphrase checking created signature failed: %s
 checking the trustdb
 cipher algorithm %d%s is unknown or disabled
 completes-needed must be greater than 0
 conflicting commands
 create ascii armored output data not saved; use option "--output" to save it
 dearmoring failed: %s
 decrypt data (default) decryption failed: %s
 decryption okay
 deleting keyblock failed: %s
 do not make any changes don't use the terminal at all enarmoring failed: %s
 encrypt data encrypted with %lu passphrases
 encrypted with 1 passphrase
 encrypted with unknown algorithm %d
 encryption only with symmetric cipher error creating passphrase: %s
 error in trailer line
 error reading keyblock: %s
 export keys export keys to a keyserver external program calls are disabled due to unsafe options file permissions
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 generate a new key pair generate a revocation certificate iImMqQsS import keys from a keyserver import/merge keys input line %u too long or missing LF
 input line longer than %d characters
 invalid S2K mode; must be 0, 1 or 3
 invalid armor header:  invalid armor: line longer than %d characters
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid default-cert-level; must be 0, 1, 2, or 3
 invalid export options
 invalid import options
 invalid min-cert-level; must be 1, 2, or 3
 invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid value
 key key export failed: %s
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key is not flagged as insecure - can't use it with the faked RNG!
 keyserver receive failed: %s
 keyserver refresh failed: %s
 keyserver search failed: %s
 keyserver send failed: %s
 keysize invalid; using %u bits
 keysize rounded up to %u bits
 list key and user IDs list keys list keys and fingerprints list keys and signatures list preferences (expert) list preferences (verbose) list secret keys make a detached signature make timestamp conflicts only a warning make_keysig_packet failed: %s
 malformed CRC
 marginals-needed must be greater than 1
 nN nested clear text signatures
 never      next trustdb check due at %s
 no no need for a trustdb check
 no remote program execution supported
 no secret key
 no signed data
 no ultimately trusted keys found
 no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 not a detached signature
 okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 original file name='%.*s'
 ownertrust information cleared
 please do a --check-trustdb
 please use "%s%s" instead
 premature eof (in CRC)
 premature eof (no CRC)
 problem handling encrypted packet
 prompt before overwriting public and secret key created and signed.
 public key decryption failed: %s
 public key encrypted data: good DEK
 qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring revocation comment:  rounded up to %u bits
 save and quit search for keys on a keyserver secret key parts are not available
 select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected digest algorithm is invalid
 show this help sign a key sign a key locally sign or edit a key signature verification suppressed
 signing failed: %s
 signing: skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 subpacket of type %d has critical bit set
 system error while calling external program: %s
 textmode the given certification policy URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 there is a secret key for public key "%s"!
 this may be caused by a missing self-signature
 this message may not be usable by %s
 trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 unable to set exec-path to %s
 unknown unnatural exit of external program
 update all keys from a keyserver update failed: %s
 update the trust database use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 user ID "%s" is already revoked
 verbose verify a signature weak key created - retrying
 weird size for an encrypted session key (%d)
 writing direct signature
 writing key binding signature
 writing self signature
 writing to stdout
 yY yes you cannot appoint a key as its own designated revoker
 |FD|write status info to this FD |NAME|use NAME as default secret key |NAME|use cipher algorithm NAME |NAME|use message digest algorithm NAME Project-Id-Version: gnupg 1.2.5
Report-Msgid-Bugs-To: translations@gnupg.org
PO-Revision-Date: 2004-07-20 15:52+0200
Last-Translator: Michal Majer <mmajer@econ.umb.sk>
Language-Team: Slovak <sk-i18n@lists.linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
Napíšte identifikátor užívateľa (user ID). Ukončite prázdnym riadkom:  
Vyberte si obrázok, ktorý bude použitý ako fotografické ID. Tento obrázok
musí byť vo formáte JPEG. Pamätajte, že bude uložený vo Vašom verejnom kľúči.
Ak použijete veľmi veľký obrázok, kľúč bude tiež veľký! Odporúčaná veľkosť
obrázka je okolo 240x288.
 
Podporované algoritmy:
                 importované: %lu                   bez zmien: %lu
               nové podkľúče: %lu
          nové id užívateľov: %lu
            neimportované: %lu
         bez identifikátorov: %lu
          Nie je isté, že tento podpis patrí vlastníkovi.
          Tento podpis je pravdepodobne FALOŠNÝ.
          Nič nenaznačuje tomu, že tento podpis patrí vlastníkovi kľúča.
                nové podpisy: %lu
       Fingerprint podkľúča:       prečítané tajné kľúče: %lu
       preskočené nové kľúče: %lu
      Fingerprint podkľúča:    (0) Neodpoviem.%s
    (1) Vôbec som to nekontroloval(a).%s
    (2) Čiastočne som to overil(a).%s
    (3) Veľmi dôkladne som to overil(a).%s
       nové revokácie kľúčov: %lu
   Nemožno podpísať.
     importované tajné kľúče: %lu
  (nexeportovateľné)  Primárny fingerprint kľúča:       tajné kľúče nezmenené: %lu
 # Zoznam pridelených hodnôt dôveryhodnosti, vytvorený %s
# (Použite "gpg --import-ownertrust" na obnovenie)
 %s ešte nepracuje s %s
 %s nedáva s %s zmysel!
 Nie je dovolené používať %s s %s!
 %s: adresár neexistuje!
 %s: chyba pri čítaní voľného záznamu: %s
 %s: chyba pri čítaní záznamu verzie: %s
 %s: chyba pri aktualizácii záznamu verzie: %s
 %s: chyba pri zápise adresárového záznamu: %s
 %s: chyba pri zápise záznamu verzie: %s
 %s: pridanie záznamu zlyhalo: %s
 %s: nepodarilo sa vytvoriť hashovaciu tabuľku: %s
 %s: nepodarilo sa vytvoriť záznam verzie: %s %s: vynulovanie záznamu zlyhalo: %s
 %s: neplatná verzia súboru %d
 %s: neplatná databáze dôvery
 %s: vytvorená neplatná databáza dôvery
 %s: súbor kľúčov (keyring) vytvorený
 %s: nie je súbor databázy dôvery
 %s: preskočené: %s
 %s: preskočené: verejný kľúč je už obsiahnutý v databáze
 %s: preskočené: verejný kľúč je neplatný (disabled)
 %s: databáza dôvery vytvorená
 %s: neznáma prípona
 %s: záznam verzie s číslom %lu
 %s:%d: použitie parametra "%s" sa neodporúča
 %s:%d: neplatný parameter pre export
 %s:%d: neplatný parameter pre import
 (Žiadny popis)
 (Pravdepodobne ste chceli vybrať %d)
 (Toto je citlivý revokačný kľúč)
 (pokiaľ neurčíte kľúč jeho fingerprintom)
 --output pre tento príkaz nefunguje
 @
(Použite manuálové stránky pre kompletný zoznam všetkých príkazov a možností)
 @
Možnosti:
  @Príkazy:
  Vynútený ASCII textový výstup.
 Ste si istý, že ho chcete stále pridať? (a/N)  Ste si istý, že ho chcete stále revokovať? (a/N)  Ste si istý, že stále chcete podpísať tento kľúč? (a/N)  Nemôžem overiť podpis: %s
 Zrušiť Certifikáty vedúce k finálnemu dôveryhodnému kľúču:
 Zmeniť (M)eno, (K)omentár, (E)-mail alebo (P)okračovať/(U)končiť?  Zmeniť (M)eno, (K)omentár, (E)-mail alebo (U)končiť?  Mením dobu platnosti primárneho kľúča.
 Šifry:  Komentár:  Kompresia:  Vytvoriť pre tento podpis revokačný certifikát? (a/N) Kritická podpisová notácia:  Kritická podpisová politika:  Zmazať tento dobrý podpis? (a/N/u) Zmazať tento neplatný podpis? (a/N/u) Zmazať tento neznámy podpis? (a/N/u) Podpis oddelený od dokumentu.
 Digest:  Chcete, aby platnosť Vášho podpisu vypršala v rovnakom čase? (A/n)  Prajete si ho zmeniť na plne exportovateľný podpis? (a/N)  Prajete si ho zmeniť na formát OpenPGP? (a/N)  Ste si istý, že stále chcete podpísať tento kľúč? (a/N)  Chcete, aby platnosť Vášho podpisu vypršala v rovnakom čase? (A/n)  E-mailová adresa:  Meno súbor s fotografiou vo formáte JPEG:  Ak chcete, napíšte popis; ukončite prázdnym riadkom:
 Vložte nový názov súboru Vložiť heslo
 Vložte heslo:  Vložte identifikátor užívateľa povereného revokáciou:  Charakteristiky:  Začnite písať svoju správu ...
 Hash:  Nápoveda: Vyberte id užívateľa na podpísanie
 S akou istotou ste preverili, že kľúč, ktorý chcete podpísať
patrí vyššie uvedenej osobe?
Pokiaľ nepoznáte odpoveď, zadajte "0".
 algoritmus IDEA nie je dostupný; optimisticky sa ho pokúsime nahradiť algoritmom %s
 Neplatný znak v komentári
 Neplatný znak ve mene
 Neplatný príkaz (skúste "help")
 Neplatný výber.
 Je táto fotografia správna (a/N/u)?  Kľúč k dispozícii na:  Vytváranie kľúča bolo zrušené.
 Vytvorenie kľúča sa nepodarilo: %s
 Kľúč bol skompromitovaný Kľúč sa už nepoužíva Kľúč revokovaný. Kľúč je nahradený Kľúč je platný na? (0)  kľúč nebol zmenený, takže nie je potrebné ho aktualizovať.
 súbor kľúčov (keyring) Na vykonanie tejto operácie je potrebný tajný kľúč.
 mMkKeEPpUu Pomoc nie je k dispozícii Dôvod nebol špecifikovaný Takýto identifikátor užívateľa neexistuje.
 Neexistuje identifikátor užívateľa s indexom %d
 Neplatná e-mailová adresa
 Poznámka: Tento kľúč bol označený ako neplatný (disabled).
 Poznámka: Skončila platnosť tohto kľúča!
 Nič nebolo zmaznané.
 Najskôr, prosím, opravte chybu
 Do poľa meno alebo komentár nepíšte, prosím, e-mailovú adresu.
 Prosím, vložte názov dátového súboru:  Prosím nezabúdajte, že zobrazované údaje o platnosti kľúčov nemusia
byť správne, pokiaľ znovu nespustíte program.
 Prosím, vyberte práve jedno id užívateľa.
 Prosím výberte dôvod na revokáciu:
 Prosím, vyberte druh kľúča, ktorý chcete:
 Prosím určte, ako dlho by mal kľúč platit.
         0 = doba platnosti kľúča nie je obmedzená
      <n>  = doba platnosti kľúča skončí za n dní
      <n>w = doba platnosti kľúča skončí za n týždňov
      <n>m = doba platnosti kľúča skončí za n mesiacov
      <n>y = doba platnosti kľúča skončí za n rokov
 Prosím určte, ako dlho by mal podpis platit.
         0 = doba platnosti podpisu nie je onmedzená
      <n>  = doba platnosti podpisu skončí za n dní
      <n>w = doba platnosti podpisu skončí za n týždňov
      <n>m = doba platnosti podpisu skončí za n mesiacov
      <n>y = doba platnosti podpisu skončí za n rokov
 Primárny fingerprint kľúča: Verejné kľúče:  Verejný kľúč je neplatný (disabled).
 Meno a priezvisko:  Skutočne vytvoriť revokačné certifikáty? (a/N)  Skutočne zmazať tento podpis podpísaný sebou samým? (a/N) Dôvod na revokáciu: %s
 Požadovaná dĺžka kľúča je %u bitov.
 Revokačný certifikát bol vytvorený.
 Bol vytvorený revokačný certifikát.

Presuňte ho na médium, ktoré môžete schovať; ak Mallory získa k
tomuto certifikátu prístup, môže znefunkčniť Váš kľúč. Jednoduché je
vytlačiť certifikát a schovať ho, pre prípad že by médium bolo nečitateľné.
Ale hrozí nebezpečenstvo: Tlačový systém Vášho počítača môže ukladať dáta a
sprístupniť ich iným!
 Tajný kľúč je dostupný.
 Tajné časti primárneho kľúča nie sú dostupné.
 Platnosť podpisu vypršala %s
 Platnosť podpisu vyprší %s
 Podpisová notácia:  Podpisová politika:  Podpis kľúča "%s" ním samým je
podpis vo formáte PGP 2.x.
 Užívateľské ID vo formáte PGP 2.x nemá žiadne predvoľby
 Tento príkaz nie je v módoch %s dovolený.
 Tento kľúč patrí nám (máme zodpovedajúci tajný kľúč)
 Tento kľúč bol označený za neplatný (disabled) Platnosť kľúča vypršala! Platnosť kľúča vyprší %s.
 Platnosť podpisu vyprší %s.
 Bude revokovaný:
 Celkovo spracovaných kľúčov: %lu
 Nekomprimované Použitie: gpgv [možnosti] [súbory] (-h pre pomoc) Užívateľské ID "%s" je revokované. ID užívateľa "%s" nie je podpísané ním samým. Užívateľské ID "%s" je revokované. Identifikátor užívateľa už neplatí VAROVÁNÍ: použitie parametra "%s" sa neodporúča
 VAROVANIE: %s prepíše %s
 VAROVANIE: Toto je PGP2 kľúč. Pridanie fotografického ID môže v niektorých
           verziách PGP viesť k odmietnutiu tohoto kľúča.
 VAROVANIE: Toto je PGP2 kľúč. Pridanie fotografického ID môže v niektorých
           verziách PGP viesť k odmietnutiu tohto kľúča.
 VAROVANIE: Tento kľúč bol revokovaný jeho určeným revokátorom/!
 VAROVANIE: Tento kľúč bol revokovaný jeho vlastníkom!
 VAROVANIE: Tento kľúč nie certifikovaný dôveryhodným podpisom!
 VAROVANIE: Tento kľúč nie je certifikovaný dostatočne dôveryhodnými podpismi!
 VAROVANIE: Tento podkľúč bol revokovaný jeho vlastníkom!
 VAROVANIE: Je použitý nedôveryhodný kľúč!
 VAROVANIE: NEdôverujeme tomuto kľúču!
 VAROVANIE: podpis použivateľkého ID vznikol %d sekund v budúcnosti
 VAROVANIE: označenie kľúča ako revokovací už nemôže byť zrušené!
 VAROVANIE: so zašifrovanou správou bolo manipulované!
 VAROVANIE: nájdený neplatný formát zápisu dátumu
 VAROVANIE: správa bola zašifrovaná slabým kľúčom v symetrickej šifre.
 VAROVANIE: správa nemá ochranu integrity
 VAROVANIE: Nájdené viacnásobne podpisy. Skontrolovaný bude len prvý.
 VAROVANIE: žiadne ID užívateľa nebolo označené ako primárne. Tento príkaz
spôsobí, že iné ID užívateľa sa bude považovať primárne.
 VAROVANIE: nič nebolo vyexportované
 VAROVANIE: pravdepodobne nebezpečný symetricky šifrovaný kľúč sedenia
 VAROVANIE: program môže vytvoriť súbor core!
 VAROVANIE: daný adresát (-r) bez použitia šifrovania s verejným kľúčom
 VAROVANIE: konflikt hashu podpisu v správe
 VAROVANIE: kľúč môže byť revokovaný (revokačný kľúč neexistuje)
 VAROVANIE: nemožno %%-expandovať notácie (príliš dlhé). Použité neexpandované.
 Je potrebné vytvoriť veľa náhodných bajtov. Počas vytvárania môžete
vykonávať inú prácu na počítači (písať na klávesnici, pohybovať myšou,
používať disky); vďaka tomu má generátor lepšiu šancu získať dostatok entropie.
 Chystáte sa revokovať tieto podpisy:
 Nemôžete zmeniť dobu platnosti kľúča verzie 3
 Nemôžete zmazať posledné id užívateľa!
 Nešpecifikovali ste identifikátor užívateľa (user ID). Môžete použiť "-r"
 Nemali by ste pridávať fotografické ID k PGP2 kľúču.
 Nemali by ste pridávať fotografické ID k PGP2 kľúču.
 Musíte vybrať aspoň jeden kľúč.
 Musíte vybrať aspoň jedno id užívateľa.
 Zvolili ste tento identifikátor užívateľa:
    "%s"

 Váš súčasný podpis na "%s"
je len lokálny.

 Váš súčasný podpis na "%s"
je len lokálny.

 Vaše rozhodnutie?  Váš výber?  Váš systém nevie zobraziť dátumy po roku 2038.
V každom prípade budú dátumy korektne spracovávané do roku 2106.
 [revokácia] [podpis kľúča ním samým] [neistý]   meno môže obsahovať len písmená, číslice, bodky, podčiarníky alebo medzery a končiť s '='
 hodnota nesmie obsahovať žiadne kontrolné znaky
 hodnota musí obsahovať znak '@'
 pridať fotografické ID pridať revokačný kľúč pridať identifikátor užívateľa ASCII hlavička:  ASCII kódovanie: %s
 automaticky odpovedať NIE na väčšinu otázok automaticky odpovedať áno na väčšinu otázok predpokladám %s šifrovaných dát
 dávkový režim: nikdy sa na nič nepýtať byť o trochu tichší binárne build_packet zlyhala: %s
 nemôžem vypnúť vytváranie core súborov: %s
 nemôžem pracovať s algoritmom verejného kľúča %d
 nemôžem pracovať s riadkami dlhšími ako %d znakov
 v móde S2K nemožno použiť symetrický ESK paket
 zrušené užívateľom
 kľúč vo formáte PGP 2.x nemožno poveriť revokáciou
 nemôžem sa vyvarovať slabého kľúča pre symetrickú šifru; operáciu som skúsil %d krát!
 zmeniť dôveryhodnosť vlastníka kľúča zmeniť heslo kontrola vytvoreného podpisu sa nepodarila: %s
 kontrolujem databázu dôvery
 šifrovací algoritmus %d%s je neznamý alebo je zakázaný
 položka completes-needed musí byť väčšia ako 0
 konfliktné príkazy
 vytvor výstup zakódovaný pomocou ASCII dáta neboli uložené; na ich uloženie použite prepínač "--output"
 dekódovanie z ASCII formátu zlyhalo: %s
 dešifrovať dáta (implicitne) dešifrovanie zlyhalo: %s
 dešifrovanie o.k.
 zmazanie bloku kľúča sa nepodarilo:  %s
 nevykonať žiadne zmeny vôbec nepoužívať terminál kódovanie do ASCII formátu zlyhalo: %s
 šifrovať dáta zašifrované s %lu heslami
 zašifrované jedným heslom
 zašifrované neznámym algoritmom %d
 šifrovanie len so symetrickou šifrou chyba pri vytváraní hesla: %s
 chyba v pätičke
 chyba pri čítaní bloku kľúča: %s
 exportovať kľúče exportovať kľúče na server kľúčov volanie externého programu zrušené kvôli nebezpečným právam súboru nastavení
 nemôžem inicializovať databázu dôvery: %s
 zlyhalo obnovenie vyrovnávacej pamäti kľúčov: %s
 vytvoriť nový pár kľúčov vytvoriť revokačný certifikát iImMuUsS importovať kľúče zo servera kľúčov importovať/zlúčiť kľúče vstupný riadok %u je príliš dlhý alebo na konci chýba znak LF
 vstupný riadok je dlhší ako %d znakov
 neplatný mód S2K; musí byť 0, 1 alebo 3
 neplatná hlavička ASCII kódovania:  neplatné kódovanie ASCII: riadok je dlhší ako %d znakov
 neplatná hlavička podpisu v čitateľnom formáte
 nesprávne označenie riadku mínusmi:  neplatné defaultné predvoľby
 neplatná implicitná úroveň certifikácie; musí byť 0, 1, 2 alebo 3
 neplatný parameter pre export
 neplatný parameter pre import
 neplatná minimálna úroveň certifikácie; musí byť 0, 1, 2 alebo 3
 neplatné užívateľské predvoľby pre šifrovanie
 neplatné užívateľské predvoľby pre kompresiu
 neplatné užívateľské predvoľby pre hashovanie
 neplatná hodnota
 key nepodaril sa export kľúča: %s
 kľúč bol vytvorený %lu sekund v budúcnosti (došlo k zmene času alebo
je problém so systémovým časom)
 kľúč bol vytvorený %lu sekund v budúcnosti (došlo k zmene času alebo
je problém so systémovým časom)
 kľúč nie je označený ako nedostatočne bezpečný - nemôžem ho použiť s falošným RNG!
 nepodarilo sa prijať kľúč zo servera: %s
 aktualizácia servera zlyhala: %s
 nepodarilo sa nájsť server: %s
 nepodarilo poslať kľúč na server: %s
 neplatná dĺžka kľúča; použijem %u bitov
 dĺžka kľúča zaokrúhlená na %u bitov
 vypísať zoznam kľúčov a id užívateľov vypísať zoznam kľúčov vypísať zoznam kľúčov a fingerprintov vypísať zoznam kľúčov a podpisov vypísať zoznam predvolieb (pre expertov) vypísať zoznam predvolieb (podrobne) vypísať zoznam tajných kľúčov vytvoriť podpis oddelený od dokumentu konflikt časového razítka make_keysig_packet zlyhala: %s
 nesprávny formát CRC
 položka marginals-needed musí byť väčšia ako 1
 nN vnorené podpisy v čitateľnom formátu
 nikdy      dalšia kontrola databázy dôvery %s
 nie nie je nutné kontrolovať databázu dôvery
 žiadne vzialené vykonávanie programu nie je podporované
 neexistuje tajný kľúč
 chýbajú podpísané dáta
 neboli nájdené žiadne absolútne dôveryhodné kľúče
 nenájdené žiadne platné dáta vo formáte OpenPGP.
 žiadne platné adresy
 nenájdený zapisovateľný súbor kľúčov (keyring): %s
 nenájdený zapisovateľný súbor verejných kľúčov (pubring): %s
 toto nie je podpis oddelený od dokumentu
 o.k., my sme anonymný adresát.
 staré kódovanie DEK nie je podporováné
 podpis starého typu (PGP 2.x)
 pôvodné meno súboru='%.*s'
 informácie o dôveryhodnosti vlastníka kľúča vymazané
 prosím vykonajte --check-trustdb
 použite namiesto neho "%s%s" 
 predčasný koniec súboru (v CRC)
 predčasný koniec súboru (žiadne CRC)
 problém so zašifrovaným paketom
 vyžiadať potvrdenie pred prepísaním verejný a tajný kľúč boli vytvorené a podpísané.
 dešifrovanie verejným kľúčom zlyhalo: %s
 dáta zašifrované verejným kľúčom: správny DEK
 uUqQ ukončiť ukončiť toto menu neplatný znak (quoted-printable) v ASCII kódovaní - pravdepodobne bol použitý nesprávny MTA
 čítam štandardný vstup (stdin) ...
 dôvod na revokáciu:  odstrániť kľúč zo súboru verejných kľúčov odstrániť kľúč zo súboru tajných kľúčov revokačná poznámka:  zaokrúhlené na %u bitov
 uložiť a ukončiť vyhľadať kľúče na serveri kľúčov tajné časti kľúča nie sú dostupné
 vyberte identifikátor užívateľa N vybraný hashovací algoritmus je neplatný
 vybraný šifrovací algoritmus je neplatný
 vybraný hashovací algoritmus je neplatný
 ukázať túto pomoc podpísať kľúč podpísať kľúč lokálne podpísať alebo modifikovať kľúč verifikácia podpisu potlačená
 podpisovanie zlyhalo: %s
 podpisujem: preskočené: verejný kľúč je už nastavený
 preskočené: verejný kľúč je už nastavený podľa implicitného adresáta
 preskočené: tajný kľúč je už v databáze
 blok typu %d bol preskočený
 samostatný revokačný certifikát -  použite "gpg --import", ak ho chcete využiť
 samostatný podpis triedy 0x%02x
 podpaket typu %d má nastavený kritický bit
 systémová chyba pri volaní externého programu: %s
 textový mód zadané URL pre certifikačnú politiku je neplatné
 zadané URL pre podpisovú politiku je neplatné
 podpis nebolo možné overiť.
Prosím, nezabúdajte, že súbor s podpisom (.sig alebo .asc)
by mal byť prvým súborom zadaným na príkazovom riadku.
 existuje tajný kľúč pre tento verejný kľúč "%s"!
 môže to byť spôsobené chýbajúcim podpisom kľúča ním samým
 táto správa nemusí použiteľná s %s
 záznam dôvery %lu nie je požadovaného typu %d
 záznam dôvery %lu, typ pož. %d: čítanie zlyhalo: %s
 záznam dôvery %lu, typ %d: zápis zlyhal: %s
 záznam v databáze dôvery %lu: lseek() sa nepodaril: %s
 záznam v databáze dôvery %lu: zápis sa nepodaril (n=%d): %s
 transakcia s databázou dôvery je príliš dlhá
 databáze dôvery: procedúra lseek() zlyhala: %s
 databáza dôvery: procedúra read() (n=%d) zlyhala: %s
 databáza dôvery: synchronizácia zlyhala %s
 nemožno nastaviť exec-path na %s
 neznáme nekorektné ukončenie externého programu
 aktualizovať všetky kľúče zo servera kľúčov aktualizácia zlyhala: %s
 aktualizovať databázu dôvery použiť ako výstupný súbor použiť kánonický textový mód aby ste ho zmazali, použite najprv prepínač "--delete-secret-key".
 užívateľské ID "%s" je už revokované
 s dodatočnými informáciami verifikovať podpis vytvorený slabý kľúč - skúšam znovu
 zvláštna veľkosť šifrovacieho kľúča pre sedenie (%d)
 zapisujem podpis kľúča ním samým (direct signature)
 zapisujem "key-binding" podpis
 zapisujem podpis kľúča sebou samým
 zapisujem na štandardný výstup (stdout)
 aAyY ano kľúč nemožno poveriť revokáciou ním samým
 |FD|zapísať informácie o stave do tohto FD |MENO|použi MENO ako implicitný tajný kľúč |ALG|použiť šifrovací algoritmus ALG |ALG|použiť hashovací algoritmus ALG 