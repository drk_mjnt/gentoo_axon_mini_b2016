# $Id: 6e57e3d7fbb0ec217b3297a93a7086bf921ffb8f $
#
# This file is sourced AFTER defaults/config.sh; generic options should be set there.
# Arch-specific options that normally shouldn't be changed.
#
KERNEL_MAKE_DIRECTIVE="Image"
KERNEL_MAKE_DIRECTIVE_2=""
KERNEL_BINARY="arch/riscv/boot/Image"
