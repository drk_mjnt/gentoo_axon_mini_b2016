<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />
<title>Docutils Internationalization</title>
<meta name="author" content="David Goodger" />
<meta name="date" content="2022-04-02" />
<meta name="copyright" content="This document has been placed in the public domain." />
<link rel="stylesheet" href="../../html4css1.css" type="text/css" />
</head>
<body>
<div class="header">
<a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../index.html">Overview</a> | <a class="reference external" href="../index.html#project-fundamentals">About</a> | <a class="reference external" href="../index.html#user">Users</a> | <a class="reference external" href="../index.html#ref">Reference</a> | <a class="reference external" href="../index.html#howto">Developers</a>
<hr class="header"/>
</div>
<div class="document" id="docutils-internationalization">
<h1 class="title"><a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> Internationalization</h1>
<table class="docinfo" frame="void" rules="none">
<col class="docinfo-name" />
<col class="docinfo-content" />
<tbody valign="top">
<tr><th class="docinfo-name">Author:</th>
<td>David Goodger</td></tr>
<tr><th class="docinfo-name">Contact:</th>
<td><a class="first last reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></td></tr>
<tr><th class="docinfo-name">Date:</th>
<td>2022-04-02</td></tr>
<tr><th class="docinfo-name">Revision:</th>
<td>9051</td></tr>
<tr><th class="docinfo-name">Copyright:</th>
<td>This document has been placed in the public domain.</td></tr>
</tbody>
</table>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-diretories.

Attention: this is not a standalone document. -->
<div class="contents topic" id="contents">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#language-module-names" id="toc-entry-1">Language Module Names</a></li>
<li><a class="reference internal" href="#docutils-language-module" id="toc-entry-2">Docutils Language Module</a></li>
<li><a class="reference internal" href="#restructuredtext-language-module" id="toc-entry-3">reStructuredText Language Module</a></li>
<li><a class="reference internal" href="#testing-the-language-modules" id="toc-entry-4">Testing the Language Modules</a></li>
<li><a class="reference internal" href="#submitting-the-language-modules" id="toc-entry-5">Submitting the Language Modules</a></li>
</ul>
</div>
<p>This document describes the internationalization facilities of the
<a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> project.  <a class="reference external" href="http://www.debian.org/doc/manuals/intro-i18n/">Introduction to i18n</a> by Tomohiro KUBOTA is a
good general reference.  &quot;Internationalization&quot; is often abbreviated
as &quot;i18n&quot;: &quot;i&quot; + 18 letters + &quot;n&quot;.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">The i18n facilities of Docutils should be considered a &quot;first
draft&quot;.  They work so far, but improvements are welcome.
Specifically, standard i18n facilities like &quot;gettext&quot; have yet to
be explored.</p>
</div>
<p>Docutils is designed to work flexibly with text in multiple languages
(one language at a time).  Language-specific features are (or should
be <a class="footnote-reference" href="#footnote-1" id="footnote-reference-1">[1]</a>) fully parameterized.  To enable a new language, two modules
have to be added to the project: one for Docutils itself (the
<a class="reference internal" href="#docutils-language-module">Docutils Language Module</a>) and one for the reStructuredText parser
(the <a class="reference internal" href="#restructuredtext-language-module">reStructuredText Language Module</a>). Users may add local language
support via a module in the PYTHONPATH root (e.g. the working directory).</p>
<table class="docutils footnote" frame="void" id="footnote-1" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#footnote-reference-1">[1]</a></td><td>If anything in Docutils is insufficiently parameterized, it
should be considered a bug.  Please report bugs to the Docutils
project bug tracker on SourceForge at
<a class="reference external" href="https://sourceforge.net/p/docutils/bugs/">https://sourceforge.net/p/docutils/bugs/</a></td></tr>
</tbody>
</table>
<div class="section" id="language-module-names">
<h1><a class="toc-backref" href="#toc-entry-1">Language Module Names</a></h1>
<p>Language modules are named using <a class="reference external" href="https://www.w3.org/International/articles/language-tags/">language tags</a> as defined in
<a class="reference external" href="https://www.rfc-editor.org/rfc/bcp/bcp47.txt">BCP 47</a>. <a class="footnote-reference" href="#footnote-2" id="footnote-reference-2">[2]</a> in lowercase, converting hyphens to underscores <a class="footnote-reference" href="#footnote-3" id="footnote-reference-3">[3]</a>.</p>
<p>A typical language identifier consists of a 2-letter language code
from <a class="reference external" href="http://www.loc.gov/standards/iso639-2/php/English_list.php">ISO 639</a> (3-letter codes can be used if no 2-letter code
exists). The language identifier can have an optional subtag,
typically for variations based on country (from <a class="reference external" href="http://www.iso.ch/iso/en/prods-services/iso3166ma/02iso-3166-code-lists/index.html">ISO 3166</a> 2-letter
country codes).  If no language identifier is specified, the default
is &quot;en&quot; for English.  Examples of module names include <tt class="docutils literal">en.py</tt>,
<tt class="docutils literal">fr.py</tt>, <tt class="docutils literal">ja.py</tt>, and <tt class="docutils literal">pt_br.py</tt>.</p>
<table class="docutils footnote" frame="void" id="footnote-2" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#footnote-reference-2">[2]</a></td><td>BCP stands for 'Best Current Practice', and is a persistent
name for a series of RFCs whose numbers change as they are updated.
The latest RFC describing language tag syntax is RFC 5646, Tags for
the Identification of Languages, and it obsoletes the older RFCs
4646, 3066 and 1766.</td></tr>
</tbody>
</table>
<table class="docutils footnote" frame="void" id="footnote-3" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#footnote-reference-3">[3]</a></td><td>Subtags are separated from primary tags by underscores instead
of hyphens, to conform to Python naming rules.</td></tr>
</tbody>
</table>
</div>
<div class="section" id="docutils-language-module">
<h1><a class="toc-backref" href="#toc-entry-2">Docutils Language Module</a></h1>
<p>Modules in <tt class="docutils literal">docutils/languages</tt> contain language mappings for
markup-independent language-specific features of Docutils.  To make a
new language module, just copy the <tt class="docutils literal">en.py</tt> file, rename it with the
code for your language (see <a class="reference internal" href="#language-module-names">Language Module Names</a> above), and
translate the terms as described below.</p>
<p>Each Docutils language module contains three module attributes:</p>
<dl class="docutils">
<dt><tt class="docutils literal">labels</tt></dt>
<dd><p class="first">This is a mapping of node class names to language-dependent
boilerplate label text.  The label text is used by Writer
components when they encounter document tree elements whose class
names are the mapping keys.</p>
<p class="last">The entry values (<em>not</em> the keys) should be translated to the
target language.</p>
</dd>
<dt><tt class="docutils literal">bibliographic_fields</tt></dt>
<dd><p class="first">This is a mapping of language-dependent field names (converted to
lower case) to canonical field names (keys of
<tt class="docutils literal">DocInfo.biblio_notes</tt> in <tt class="docutils literal">docutils.transforms.frontmatter</tt>).
It is used when transforming bibliographic fields.</p>
<p class="last">The keys should be translated to the target language.</p>
</dd>
<dt><tt class="docutils literal">author_separators</tt></dt>
<dd><p class="first">This is a list of strings used to parse the 'Authors'
bibliographic field.  They separate individual authors' names, and
are tried in order (i.e., earlier items take priority, and the
first item that matches wins).  The English-language module
defines them as <tt class="docutils literal"><span class="pre">[';',</span> <span class="pre">',']</span></tt>; semi-colons can be used to
separate names like &quot;Arthur Pewtie, Esq.&quot;.</p>
<p class="last">Most languages won't have to &quot;translate&quot; this list.</p>
</dd>
</dl>
</div>
<div class="section" id="restructuredtext-language-module">
<h1><a class="toc-backref" href="#toc-entry-3">reStructuredText Language Module</a></h1>
<p>Modules in <tt class="docutils literal">docutils/parsers/rst/languages</tt> contain language
mappings for language-specific features of the reStructuredText
parser.  To make a new language module, just copy the <tt class="docutils literal">en.py</tt> file,
rename it with the code for your language (see <a class="reference internal" href="#language-module-names">Language Module
Names</a> above), and translate the terms as described below.</p>
<p>Each reStructuredText language module contains two module attributes:</p>
<dl class="docutils">
<dt><tt class="docutils literal">directives</tt></dt>
<dd><p class="first">This is a mapping from language-dependent directive names to
canonical directive names.  The canonical directive names are
registered in <tt class="docutils literal">docutils/parsers/rst/directives/__init__.py</tt>, in
<tt class="docutils literal">_directive_registry</tt>.</p>
<p class="last">The keys should be translated to the target language.  Synonyms
(multiple keys with the same values) are allowed; this is useful
for abbreviations.</p>
</dd>
<dt><tt class="docutils literal">roles</tt></dt>
<dd><p class="first">This is a mapping language-dependent role names to canonical role
names for interpreted text.  The canonical directive names are
registered in <tt class="docutils literal">docutils/parsers/rst/states.py</tt>, in
<tt class="docutils literal">Inliner._interpreted_roles</tt> (this may change).</p>
<p class="last">The keys should be translated to the target language.  Synonyms
(multiple keys with the same values) are allowed; this is useful
for abbreviations.</p>
</dd>
</dl>
</div>
<div class="section" id="testing-the-language-modules">
<h1><a class="toc-backref" href="#toc-entry-4">Testing the Language Modules</a></h1>
<p>Whenever a new language module is added or an existing one modified,
the unit tests should be run.  The test modules can be found in the
docutils/test directory from <a class="reference external" href="https://sourceforge.net/p/docutils/code/HEAD/tree/trunk/">code</a> or from the <a class="reference external" href="https://sourceforge.net/p/docutils/code/HEAD/tarball">latest snapshot</a>.</p>
<p>The <tt class="docutils literal">test_language.py</tt> module can be run as a script.  With no
arguments, it will test all language modules.  With one or more
language codes, it will test just those languages.  For example:</p>
<pre class="literal-block">
$ python test_language.py en
..
----------------------------------------
Ran 2 tests in 0.095s

OK
</pre>
<p>Use the &quot;alltests.py&quot; script to run all test modules, exhaustively
testing the parser and other parts of the Docutils system.</p>
</div>
<div class="section" id="submitting-the-language-modules">
<h1><a class="toc-backref" href="#toc-entry-5">Submitting the Language Modules</a></h1>
<p>If you do not have repository write access and want to contribute your
language modules, feel free to submit them via the <a class="reference external" href="https://sourceforge.net/p/docutils/patches/">SourceForge patch
tracker</a>.</p>
</div>
</div>
<div class="footer">
<hr class="footer" />
<a class="reference external" href="i18n.txt">View document source</a>.
Generated on: 2022-10-16 21:27 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.

</div>
</body>
</html>
