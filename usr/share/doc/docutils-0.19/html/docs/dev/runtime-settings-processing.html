<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />
<title>Runtime Settings Processing</title>
<meta name="author" content="David Goodger, Günter Milde" />
<meta name="date" content="2022-04-02" />
<meta name="copyright" content="This document has been placed in the public domain." />
<link rel="stylesheet" href="../../html4css1.css" type="text/css" />
</head>
<body>
<div class="header">
<a class="reference external" href="https://docutils.sourceforge.io">Docutils</a> | <a class="reference external" href="../index.html">Overview</a> | <a class="reference external" href="../index.html#project-fundamentals">About</a> | <a class="reference external" href="../index.html#user">Users</a> | <a class="reference external" href="../index.html#ref">Reference</a> | <a class="reference external" href="../index.html#howto">Developers</a>
<hr class="header"/>
</div>
<div class="document" id="runtime-settings-processing">
<h1 class="title">Runtime Settings Processing</h1>
<table class="docinfo" frame="void" rules="none">
<col class="docinfo-name" />
<col class="docinfo-content" />
<tbody valign="top">
<tr><th class="docinfo-name">Author:</th>
<td>David Goodger, Günter Milde</td></tr>
<tr><th class="docinfo-name">Contact:</th>
<td><a class="first last reference external" href="mailto:docutils-develop&#64;lists.sourceforge.net">docutils-develop&#64;lists.sourceforge.net</a></td></tr>
<tr><th class="docinfo-name">Date:</th>
<td>2022-04-02</td></tr>
<tr><th class="docinfo-name">Revision:</th>
<td>9051</td></tr>
<tr><th class="docinfo-name">Copyright:</th>
<td>This document has been placed in the public domain.</td></tr>
</tbody>
</table>
<div class="abstract topic">
<p class="topic-title">Abstract</p>
<p>A detailled description of Docutil's settings processing
framework.</p>
</div>
<!-- Minimal menu bar for inclusion in documentation sources
in ``docutils/docs/*/`` sub-diretories.

Attention: this is not a standalone document. -->
<div class="contents topic" id="contents">
<p class="topic-title">Contents</p>
<ul class="simple">
<li><a class="reference internal" href="#settings-priority" id="toc-entry-1">Settings priority</a></li>
<li><a class="reference internal" href="#runtime-settings-processing-for-command-line-tools" id="toc-entry-2">Runtime settings processing for command-line tools</a></li>
<li><a class="reference internal" href="#runtime-settings-processing-for-other-applications" id="toc-entry-3">Runtime settings processing for other applications</a></li>
</ul>
</div>
<p>The <tt class="docutils literal">docutils/__init__.py</tt>, <tt class="docutils literal">docutils/core.py</tt>, and
<tt class="docutils literal">docutils.frontend.py</tt> modules are described.
Following along with the actual code is recommended.</p>
<p>See <a class="reference external" href="../api/runtime-settings.html">Docutils Runtime Settings</a> for a high-level description of the core
API and <a class="reference external" href="../user/config.html#configuration-files">Docutils Configuration</a> for a description of the individual
settings.</p>
<div class="admonition note">
<p class="first admonition-title">Note</p>
<p class="last">This document is informal.
It describes the state in Docutils 0.18.1.
Implementation details will change with the move to replace the
deprecated <a class="reference external" href="https://docs.python.org/dev/library/optparse.html">optparse</a> module with <a class="reference external" href="https://docs.python.org/dev/library/argparse.html">argparse</a>.</p>
</div>
<div class="section" id="settings-priority">
<h1><a class="toc-backref" href="#toc-entry-1">Settings priority</a></h1>
<p>Docutils overlays default and explicitly specified values from various
sources such that settings behave the way we want and expect them to
behave.</p>
<p>The souces are overlaid in the following order (later sources
overwrite earlier ones):</p>
<ol class="arabic">
<li><p class="first">Defaults specified in <a class="reference external" href="../api/runtime-settings.html#settingsspec-settings-spec">settings_spec</a> and
<a class="reference external" href="../api/runtime-settings.html#settingsspec-settings-defaults">settings_defaults</a> attributes for each <a class="reference external" href="../api/runtime-settings.html#components">component</a>.
(<a class="reference internal" href="#optionparser-populate-from-components">details</a>)</p>
</li>
<li><p class="first">Defaults specified in <a class="reference external" href="../api/runtime-settings.html#settingsspec-settings-default-overrides">settings_default_overrides</a> attribute for
each <a class="reference external" href="../api/runtime-settings.html#components">component</a>.
(<a class="reference internal" href="#component-settings-default-overrides">details</a>)</p>
</li>
<li><p class="first">Settings specified in the <a class="reference external" href="../api/runtime-settings.html#settings-overrides-parameter">settings_overrides parameter</a> of the
<a class="reference external" href="../api/runtime-settings.html#convenience-functions">convenience functions</a> rsp. the <cite>settings_overrides</cite> attribute of
a <cite>core.Publisher</cite> instance.
(<a class="reference internal" href="#optionparser-defaults">details</a>)</p>
</li>
<li><p class="first">If enabled, settings specified in <a class="reference external" href="../api/runtime-settings.html#active-sections">active sections</a> of the
<a class="reference external" href="../user/config.html#configuration-files">configuration files</a> in the order described in
<a class="reference external" href="../user/config.html#configuration-file-sections-entries">Configuration File Sections &amp; Entries</a>. (<a class="reference internal" href="#optionparser-get-standard-config-settings">details</a>)</p>
<p>See also <a class="reference external" href="../user/config.html#configuration-file-sections-entries">Configuration File Sections &amp; Entries</a>.</p>
</li>
<li><p class="first">If enabled, command line arguments (<a class="reference internal" href="#publisher-process-command-line">details</a>).</p>
</li>
</ol>
<p>Settings assigned to the <a class="reference external" href="../api/runtime-settings.html#settings-parameter">settings parameter</a> of the
<a class="reference external" href="../api/runtime-settings.html#convenience-functions">convenience functions</a> or the <tt class="docutils literal">Publisher.settings</tt> attribute
are used <strong>instead of</strong> the above sources
(see below for details for <a class="reference internal" href="#publisher-publish">command-line tools</a> and
<a class="reference internal" href="#publisher-process-programmatic-settings">other applications</a>).</p>
</div>
<div class="section" id="runtime-settings-processing-for-command-line-tools">
<span id="command-line-tools"></span><h1><a class="toc-backref" href="#toc-entry-2">Runtime settings processing for command-line tools</a></h1>
<p>The command-line <a class="reference external" href="../user/tools.html">front-end tools</a> usually import and call
the <a class="reference external" href="../api/runtime-settings.html#convenience-functions">convenience function</a> <tt class="docutils literal">docutils.core.publish_cmdline()</tt>.</p>
<ol class="arabic">
<li><p class="first"><tt class="docutils literal">docutils.core.publish_cmdline()</tt> creates a <a class="reference external" href="../peps/pep-0258.html#publisher">Publisher</a> instance:</p>
<pre class="literal-block">
publisher = core.Publisher(…, settings)
</pre>
<p>eventually sets the <a class="reference external" href="../api/runtime-settings.html#components">components</a> from the respective names, and calls</p>
<pre class="literal-block">
publisher.publish(argv, …, settings_spec,
                  settings_overrides, config_section, …)
</pre>
</li>
<li id="publisher-publish"><p class="first">If <cite>publisher.settings</cite> is None, <tt class="docutils literal">publisher.publish()</tt> calls:</p>
<pre class="literal-block">
publisher.process_command_line(…,
    settings_spec, config_section, **defaults)
</pre>
<p>with <cite>defaults</cite> taken from <cite>publisher.settings_overrides</cite>.</p>
<p>If <cite>publisher.settings</cite> is defined, steps 3&nbsp;to&nbsp;5 are skipped.</p>
</li>
<li><p class="first"><tt class="docutils literal">publisher.process_command_line()</tt> calls:</p>
<pre class="literal-block">
optpar = publisher.setup_option_parser(…,
             settings_spec, config_section, **defaults)
</pre>
</li>
<li id="publisher-setup-option-parser"><p class="first"><tt class="docutils literal">publisher.setup_option_parser()</tt></p>
<ul>
<li><p class="first">merges the value of the <a class="reference external" href="../api/runtime-settings.html#config-section-parameter">config_section parameter</a> into
<cite>settings_spec</cite> and</p>
</li>
<li><p class="first">creates an <cite>OptionParser</cite> instance</p>
<pre class="literal-block">
optpar = docutils.frontend.OptionParser(components, defaults)
</pre>
<p>with <cite>components</cite> the tuple of the <a class="reference external" href="../api/runtime-settings.html#settingsspec">SettingsSpec</a> instances
<tt class="docutils literal">(publisher.parser, publisher.reader, publisher.writer, settings_spec)</tt></p>
</li>
</ul>
</li>
<li id="optionparser-populate-from-components"><p class="first">The <cite>OptionParser</cite> instance prepends itself to the <cite>components</cite> tuple
and calls <tt class="docutils literal">self.populate_from_components(components)</tt>, which updates
the attribute <tt class="docutils literal">self.defaults</tt> in two steps:</p>
<ol class="loweralpha simple">
<li>For each component passed, <tt class="docutils literal">component.settings_spec</tt> is processed
and <tt class="docutils literal">component.settings_defaults</tt> is applied.</li>
<li id="component-settings-default-overrides">In a second loop, for each component
<tt class="docutils literal">component.settings_default_overrides</tt> is applied. This way,
<tt class="docutils literal">component.settings_default_overrides</tt> can override the default
settings of any other component.</li>
</ol>
</li>
<li id="optionparser-defaults"><p class="first">Back in <tt class="docutils literal">OptionParser.__init__()</tt>, <tt class="docutils literal">self.defaults</tt> is updated with
the <cite>defaults</cite> argument passed to <tt class="docutils literal"><span class="pre">OptionParser(…)</span></tt> in step 5.</p>
<p>This means that the <cite>settings_overrides</cite> argument of the
<a class="reference external" href="../api/runtime-settings.html#convenience-functions">convenience functions</a> has priority over all
<tt class="docutils literal">SettingsSpec.settings_spec</tt> defaults.</p>
</li>
<li id="optionparser-get-standard-config-settings"><p class="first">If configuration files are enabled,
<tt class="docutils literal">self.get_standard_config_settings()</tt> is called.</p>
<p>This reads the Docutils <a class="reference external" href="../user/config.html#configuration-files">configuration files</a>, and returns a
dictionary of settings in <a class="reference external" href="../api/runtime-settings.html#active-sections">active sections</a> which is used to update
<tt class="docutils literal">optpar.defaults</tt>. So configuration file settings have priority over
all software-defined defaults.</p>
</li>
<li id="publisher-process-command-line"><p class="first"><tt class="docutils literal">publisher.process_command_line()</tt> calls <tt class="docutils literal">optpar.parse_args()</tt>.
The OptionParser parses all command line options and returns a
<cite>docutils.frontend.Values</cite> object.
This is assigned to <tt class="docutils literal">publisher.settings</tt>.
So command-line options have priority over configuration file
settings.</p>
</li>
<li><p class="first">The <cite>&lt;source&gt;</cite> and <cite>&lt;destination&gt;</cite> command-line arguments
are also parsed, and assigned to <tt class="docutils literal">publisher.settings._source</tt>
and <tt class="docutils literal">publisher.settings._destination</tt> respectively.</p>
</li>
<li><p class="first"><tt class="docutils literal">publisher.publish()</tt> calls <tt class="docutils literal">publisher.set_io()</tt> with no arguments.
If either <tt class="docutils literal">publisher.source</tt> or <tt class="docutils literal">publisher.destination</tt> are not
set, the corresponding <tt class="docutils literal">publisher.set_source()</tt> and
<tt class="docutils literal">publisher.set_destination()</tt> are called:</p>
<dl class="docutils">
<dt><tt class="docutils literal">publisher.set_source()</tt></dt>
<dd><p class="first last">checks for a <tt class="docutils literal">source_path</tt> argument, and if there is none (which
is the case for command-line use), it is taken from
<tt class="docutils literal">publisher.settings._source</tt>.  <tt class="docutils literal">publisher.source</tt> is set by
instantiating a <tt class="docutils literal">publisher.source_class</tt> object.
For command-line front-end tools, the default
<tt class="docutils literal">publisher.source_class</tt> (i.e. <tt class="docutils literal">docutils.io.FileInput</tt>)
is used.</p>
</dd>
<dt><tt class="docutils literal">publisher.set_destination()</tt></dt>
<dd><p class="first last">does the same job for the destination. (the default
<tt class="docutils literal">publisher.destination_class</tt> is <tt class="docutils literal">docutils.io.FileOutput</tt>).</p>
</dd>
</dl>
</li>
<li id="accessing-the-runtime-settings"><p class="first"><tt class="docutils literal">publisher.publish()</tt> passes <tt class="docutils literal">publisher.settings</tt> to the <a class="reference external" href="../peps/pep-0258.html#reader">reader</a>
component's <tt class="docutils literal">read()</tt> method.</p>
</li>
<li><p class="first">The reader component creates a new <a class="reference external" href="../ref/doctree.html#document">document root node</a>.
<tt class="docutils literal">nodes.document.__init__()</tt> adds the settings to the internal
attributes.</p>
<p>All components acting on the Document Tree are handed the
<tt class="docutils literal">document</tt> root node and can access the runtime settings as
<tt class="docutils literal">document.settings</tt>.</p>
</li>
</ol>
</div>
<div class="section" id="runtime-settings-processing-for-other-applications">
<h1><a class="toc-backref" href="#toc-entry-3">Runtime settings processing for other applications</a></h1>
<p>The <a class="reference external" href="../api/runtime-settings.html#convenience-functions">convenience functions</a> , <tt class="docutils literal">core.publish_file()</tt>,
<tt class="docutils literal">core.publish_string()</tt>, or <tt class="docutils literal">core.publish_parts()</tt> do not parse the
command line for settings.</p>
<ol class="arabic">
<li><p class="first">The convenience functions call the generic programmatic interface
function <tt class="docutils literal">core.publish_programmatically()</tt> that creates a
<cite>core.Publisher</cite> instance</p>
<pre class="literal-block">
pub = core.Publisher(…, settings)
</pre>
<p>eventually sets the <a class="reference external" href="../api/runtime-settings.html#components">components</a> from the respective names, and calls</p>
<pre class="literal-block">
publisher.process_programmatic_settings(
    settings_spec, settings_overrides, config_section)
</pre>
</li>
<li id="publisher-process-programmatic-settings"><p class="first">If <cite>publisher.settings</cite> is None,
<tt class="docutils literal">publisher.process_programmatic_settings()</tt> calls:</p>
<pre class="literal-block">
publisher.get_settings(settings_spec, config_section, **defaults)
</pre>
<p>with <cite>defaults</cite> taken from <cite>publisher.settings_overrides</cite>.</p>
<p>If <cite>publisher.settings</cite> is defined, the following steps are skipped.</p>
</li>
<li><p class="first"><tt class="docutils literal">publisher.get_settings()</tt> calls:</p>
<pre class="literal-block">
optpar = publisher.setup_option_parser(…,
             settings_spec, config_section, **defaults)
</pre>
</li>
<li><p class="first">The OptionParser instance determines setting defaults
as described in <a class="reference internal" href="#publisher-setup-option-parser">steps 4&nbsp;to&nbsp;7</a> in the previous section.</p>
</li>
<li><p class="first">Back in <tt class="docutils literal">publisher.get_settings()</tt>, the <tt class="docutils literal">frontend.Values</tt> instance
returned by <tt class="docutils literal">optpar.get_default_values()</tt> is stored in
<tt class="docutils literal">publisher.settings</tt>.</p>
</li>
<li><p class="first"><tt class="docutils literal">publish_programmatically()</tt> continues with setting
<tt class="docutils literal">publisher.source</tt> and <tt class="docutils literal">publisher.destination</tt>.</p>
</li>
<li><p class="first">Finally, <tt class="docutils literal">publisher.publish()</tt> is called. As <tt class="docutils literal">publisher.settings</tt>
is not None, no further command line processing takes place.</p>
</li>
<li><p class="first">All components acting on the Document Tree are handed the
<tt class="docutils literal">document</tt> root node and can access the runtime settings as
<tt class="docutils literal">document.settings</tt> (cf. <a class="reference internal" href="#accessing-the-runtime-settings">steps 11 and 12</a> in the previous section).</p>
</li>
</ol>
<!-- References: -->
</div>
</div>
<div class="footer">
<hr class="footer" />
<a class="reference external" href="runtime-settings-processing.txt">View document source</a>.
Generated on: 2022-10-16 21:27 UTC.
Generated by <a class="reference external" href="https://docutils.sourceforge.io/">Docutils</a> from <a class="reference external" href="https://docutils.sourceforge.io/rst.html">reStructuredText</a> source.

</div>
</body>
</html>
